// controller
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// User Registration

module.exports.registerUser = async (reqbody) => {
  try {

    // Check if the email is already in use
    const existingUser = await User.findOne({ email: reqbody.email });
    
    if (existingUser) {
      return "Email is already in use."; // Return message if the email is already in use
    }

    // If the email is not in use, proceed with registration
    let newUser = new User({
      firstName: reqbody.firstName,
      lastName: reqbody.lastName,
      email: reqbody.email,
      mobileNo: reqbody.mobileNo,
      password: bcrypt.hashSync(reqbody.password,10),
   	  address: [
   	  		{
        		barangay: reqbody.barangay,
        		city: reqbody.city,
        		province: reqbody.province,
        		country: reqbody.country
      		}
      ]
    });

    const user = await newUser.save();
    return true; // Return true upon successful user registration

  } catch (error) {
    console.error('Error registering user:', error);
    return false; // Return false if an error occurs during user registration
  }
};


// user Authentication
module.exports.loginUser = (req, res) => {
  User.findOne({ email: req.body.email })
    .then((result) => {
      if (result === null) {
      	console.log(result)
        res.send(false); // Return false if user not found
      } else {
        const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

        if (isPasswordCorrect) {
          const accessToken = auth.createAccessToken(result);
          res.send({ access: accessToken }); // Return the access token if password matches
        } else {
        	console.log(result)
          res.send("Incorrect password or email"); // Return false if password doesn't match
        }
      }
    })
    .catch((err) => {
      console.error('Error during user login:', err);
      res.status(500).json({ message: 'An error occurred during login' }); // Send a proper error response
    });
};

// Create Order

module.exports.order = async (req, res) => {
  try {
    // Check if the user is an admin and deny the checkout
    if (req.user.isAdmin) {
      return res.status(403).send("Action Forbidden"); 
    }

    const { productId, quantity } = req.body; // Extract productId and quantity from request body

    // Fetch the product from the database to get its price
    const product = await Product.findById(productId);

    if (!product) {
      return res.status(404).send({ message: "Product not found" });
    }

    const model = product.model;
    const price = product.price;
    const totalPrice = product.price * quantity; // Calculate the total price based on quantity and product's price

    // Check if the ordered quantity is available in stock
    if (quantity > product.stock) {
      return res.status(400).send({ message: "Insufficient stock" });
    }

    // Update the user's orderedProducts array with productId, quantity, and totalAmount
    const updatedUser = await User.findByIdAndUpdate(
      req.user.id,
      { $push: 
        { 
          orderedProducts: 
            { products: { productId, productModel: model , price: price , quantity,},
            subTotal: totalPrice } } },
      { new: true }
    );

    if (!updatedUser) {
      return res.status(500).send({ message: "Error updating user" });
    }

    // Update the product's userOrders array with userId
    const updatedProduct = await Product.findByIdAndUpdate(
      productId,
      { $push: { userOrders: { userId: req.user.id } }, $inc: { stock: - quantity } },
      { new: true }
    );

    if (!updatedProduct) {
      return res.status(500).send({ message: "Error updating product" });
    }

    return res.send({ message: "Purchase Successfully" });
  }

  catch (error) {
    console.error('Error during order:', error);
    res.status(500).json({ message: 'An error occurred during order' });
  }
};


// Retrieve user details
module.exports.getUserDetails = (req,res) => {
  
  return User.findById(req.user.id).then(result => {
      result.password = "******";
      return res.send({ userDetails: result })
  })

  .catch(error => err)

}


//Update user as admin 
module.exports.updateUserAsAdmin = async (req, res) => {
  try {
    const { userId } = req.body;
  
    // Find the user and update isAdmin flag
    const user = await User.findById(userId);
  
    if (!user) {
    return res.status(404).json({ error: 'User not found' });
    }
  
    user.isAdmin = true;
  
    // Save the updated user document
    await user.save();
  
    res.status(200).json({ message: 'User updated as admin successfully' });
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while updating the user as admin' });
  }
};



// Retrieve authenticated users orders

module.exports.getUserOrders = (req,res) => {
  
  return User.findById(req.user.id).then(result => {
      
      return res.send({ Orders: result.orderedProducts })
  })

  .catch(error => err)

}

// Retrieve all users orders (admin only)

module.exports.getAllOrders = (req, res) => {
  User.find({})
    .then(users => {
      const allOrderedProducts = [];
      
      users.forEach(user => {
        allOrderedProducts.push(...user.orderedProducts);
      });
      
      return res.send(allOrderedProducts);
    })
    .catch(error => err)
};



// Add to cart

module.exports.addToCart = async (req,res) => {
  try {
    // Check if the user is an admin and deny the add to cart
    if(req.user.isAdmin) {
      return res.status(403).send("Action Forbidden");
    }

    const { quantity } = req.body; //Extract quantity from req body
    const productId = req.params.productId;


    // Fetch the product from the database to get its price
    const product = await Product.findById(productId);

    if (!product) {
      return res.status(404).send ({ message: "Product not found"});
    }

    // Calculate the total price based on quantity and product's price
    const subTotal = product.price * quantity;
    const model = product.model;
    const price = product.price;

    // Check if the ordered quantity is available in stock
    if (quantity > product.stock) {
      return res.status(400).send({ message: "Insufficient stock" });
    }

     // Update the user's cartItem array with productId, quantity, and totalAmount
    const updatedUser = await User.findByIdAndUpdate(
      req.user.id,
      { $push: { 
        cartItem:{ 
          products:
          {productId, 
          quantity,
          price: price,
          productModel: model,},
          subTotal: subTotal
           } } },
      { new: true }
    );

     if (!updatedUser) {
      return res.status(500).send({ message: "Error updating user" });
    }

    return res.send({ message: "Add to cart Successfully" });

  } catch (error) {
    res.status(500).json({ error: 'An error occurred while Adding item to cart' });
  }

  
}


// update cart's item quantity

module.exports.updateCartItemQuantity = async (req, res) => {
  try {
    const userId = req.user.id; // Extract user ID from the authenticated user's token
    const productId = req.params.productId; // Extract the product ID from the route parameters
    const newQuantity = req.body.quantity; // Extract the new quantity from the request body

    // Find the user document based on the extracted user ID
    const user = await User.findById(userId);

    // Check if the user exists
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Find the cart item within the user's cart based on the extracted product ID
    const cartItemToEdit = user.cartItem.find(item => item.productId === productId);

    // Check if the cart item exists
    if (!cartItemToEdit) {
      return res.status(404).json({ message: 'Cart item not found' });
    }

    // Find the corresponding product in the database based on the product ID
    const product = await Product.findById(productId);

    // Check if the product exists
    if (!product) {
      return res.status(404).json({ message: 'Product not found' });
    }

    // Check if the new quantity is zero
    if (newQuantity === 0) {
      return res.status(400).json({ message: 'Invalid quantity' });
    }

    // Check if the new quantity exceeds the available stock for the product
    if (newQuantity > product.stock) {
      return res.status(400).json({ message: 'Insufficient stock' });
    }

    // Update the cart item's quantity and subTotal based on the new quantity and product price
    cartItemToEdit.quantity = newQuantity;
    cartItemToEdit.subTotal = newQuantity * product.price;

    // Save the updated user document with the modified cart item
    await user.save();

    // Respond with a success message and the updated cart items
    res.status(200).json({ message: 'Cart item quantity updated successfully', cart: user.cartItem });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};



// Remove Item from cart

module.exports.deleteItem = (req, res) => {
  const userId = req.user.id; 
  const productId = req.params.productId;

  User.findByIdAndUpdate(
    userId,
    { $pull: { cartItem: { productId } } },
    { new: true }
  )
    .then(updatedUser => {
      if (!updatedUser) {
        return res.status(404).send(false);
      }
      return res.send(true);
    })
    .catch(error => {
      console.error(error);
      return res.status(500).send(false);
    });
};


// Sum of all item in cart
module.exports.total = async (req, res) => {
  try {
    const userId = req.user.id;

    // Find the user in the database using their ID
    const user = await User.findById(userId);

    // Check if the user exists
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Calculate the total price of all items in the cart using the reduce method
    const Total = user.cartItem.reduce((total, cartItem) => total + cartItem.subTotal, 0);

    // Send the calculated cart total as a response
    res.status(200).json({ Total });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Oops! Something went wrong on our side.' });
  }
};


//  UPDATING USERS INFO

// reset password
module.exports.resetPassword = async (req, res) => {
  try {
    const { currentPassword, newPassword } = req.body; // Combine destructuring into one line
    const { id } = req.user; // Extracting user ID from the authorization header

    // Fetch the user from the database
    const user = await User.findById(id);

    if (!user) {
      return res.status(404).json({ message: 'User not found' }); // User not found
    }

    // Compare the current password with the user's stored password
    const passwordMatch = await bcrypt.compare(currentPassword, user.password);

    if (!passwordMatch) {
      return res.status(400).send("Incorrect Password");
    }

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json({ message: 'Password reset successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

// update the user profile
module.exports.updateProfile = async (req, res) => {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNo, address } = req.body; // Destructure the address object

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo, address }, 
      { new: true }
    );

    // Return the updated profile information
    res.json({
      firstName: updatedUser.firstName,
      lastName: updatedUser.lastName,
      mobileNo: updatedUser.mobileNo,
      address: updatedUser.address 
    });

  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Failed to update profile' });
  }
};
