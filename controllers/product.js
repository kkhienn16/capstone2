// controller
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");

// Add product
module.exports.addProduct = (req,res) => {
	
	let newProduct = new Product ({
		brand : req.body.brand,
		model : req.body.model,
		size : req.body.size,
		description : req.body.description,
		price : req.body.price,
		stock: req.body.stock
	})

	// saves the created object to our database
	return newProduct.save().then((product,error) => {

		//  Course creation failed
		if(error){
			return res.send(false)

		// Course creation successful
		} else {
			return res.send(true)
		}
	})
	.catch(err => res.send(err))
};

// Get active products
module.exports.getAllActiveProducts = (req,res) => {
	Product.find({isActive:true}).then(products => {
		   // Create an array to store the formatted product data
    const formattedProducts = products.map(product => ({
      brand: product.brand,
      model: product.model,
      description: product.description,
      size: product.size,
      price: product.price,
      stock: product.stock
    }));

    // Return the formatted products
    res.json(formattedProducts);
	})
}


// Get all products
module.exports.getAllProducts = (req, res) => {
  Product.find({}).then(products => {
    // Create an array to store the formatted product data
    const formattedProducts = products.map(product => ({
      brand: product.brand,
      model: product.model,
      description: product.description,
      size: product.size,
      price: product.price,
      stock: product.stock,
      isActive: product.isActive
    }));

    // Return the formatted products
    res.json(formattedProducts);
  })
  .catch(error => {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  });
};

// Retrieve a single product
module.exports.getProduct = (req, res) => {
  Product.findById(req.params.productId)
    .then(product => {
      if (!product) {
        return res.status(404).json({ message: 'Product not found' });
      }

      // Format the product data
      const formattedProduct = {
        brand: product.brand,
        model: product.model,
        description: product.description,
        size: product.size,
        price: product.price,
        stock: product.stock
      };

      // Return the formatted product
      res.json(formattedProduct);
    })
    .catch(error => {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    });
};
// updating a product information

module.exports.updateProduct = (req,res) => {

	let updatedProduct = {
		brand: req.body.brand,
		model: req.body.model,
		size: req.body.size,
		description: req.body.description,
		price: req.body.price,
		stock: req.body.stock
	}

	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product,error) =>{
		if(error){
			return res.send(false);
		}else {
			return res.send(true);
		}
	})
}

// Archive and Activate product
module.exports.productAvailabilty = (req,res) => {
	let availability = {
		isActive: req.body.isActive
	}
	return Product.findByIdAndUpdate(req.params.productId, availability).then((product,error) => {
		if(error){
			return res.send(false);
		} else {
			return res.send(true);
		}
	})

} 

// filter products by price 
module.exports.filterProductByPrice = async (req, res) => {
  try {
    const { minPrice, maxPrice } = req.body;

    // Query products within the given price range
    const products = await Product.find({
      price: { $gte: minPrice, $lte: maxPrice }
    });

    // Create an array to store the formatted product data
    const formattedProducts = products.map(product => ({
      brand: product.brand,
      model: product.model,
      description: product.description,
      size: product.size,
      price: product.price,
      stock: product.stock
    }));

    res.json(formattedProducts);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


// Filter products by name
module.exports.searchProductsByName = async (req, res) => {
  try {
    const { productName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const products = await Product.find({
      brand: { $regex: productName, $options: 'i' }
    });

    if (products.length === 0) {
      return res.status(404).json({ message: 'Results not found' });
    }

    // Create an array to store the formatted product data
    const formattedProducts = products.map(product => ({
      brand: product.brand,
      model: product.model,
      description: product.description,
      size: product.size,
      price: product.price,
      stock: product.stock
    }));

    res.json(formattedProducts);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};




  // res.json({
  //     firstName: updatedUser.firstName,
  //     lastName: updatedUser.lastName,
  //     mobileNo: updatedUser.mobileNo,
  //     address: updatedUser.address 
  //   });