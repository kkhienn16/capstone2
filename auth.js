const jwt = require("jsonwebtoken");
const secret = "eCommerceAPI";

// Token Creation

module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };

  return jwt.sign(data, secret);
};

// Token verification

module.exports.verify = (req, res, next) => {
  console.log("This is from req.headers.authorization");
  console.log(req.headers.authorization);
  let token = req.headers.authorization;

  if (typeof token === "undefined") {
    return res.status(401).json({ auth: "Failed. No Token" }); // Use status code 401 for unauthorized
  } else {
    console.log("With bearer prefix");
    console.log(token);
    token = token.slice(7); // Remove "Bearer " prefix

    console.log("No bearer prefix");
    console.log(token);

    // Token decryption
    jwt.verify(token, secret, (err, decodedToken) => {
      if (err) {
        return res.status(401).json({ auth: "Failed", message: err.message });
      } else {
        console.log("data that will be assigned to the req.user");
        req.user = decodedToken;
        next();
      }
    });
  }
};

module.exports.verifyAdmin = (req, res, next) => {
  // verifyAdmin comes AFTER the verify middleware
  if (req.user.isAdmin) {
    next();
  } else {
    return res.status(403).json({
      auth: "Failed",
      message: "Action Forbidden", // Use status code 403 for forbidden actions
    });
  }
};
