## E-COMMERCE API DOCUMENTATION

***TEST ACCOUNTS:***
- Regular User:
     - email: walter@mail.com
     - pwd: password123
- Admin User:
    - email: gus@mail.com
    - pwd: admin123
    

***ROUTES:***
- User registration (POST)
	- http://localhost:4000/users/register
    - request body: 
        - firstName (string)
        - lastName (string)
        - email (string)
        - password (string)
        - mobileNo (string)
        - barangay (string)
        - city (string)
        - province (string)
        - country (string)

- User authentication (POST)
	- http://localhost:4000/users/login
    - request body: 
        - email (string)
        - password (string)

- Create Product (Admin only) (POST)
	- http://localhost:4000/products/create
    - request body: 
        - brand (string)
        - model (string)
        - size (string)
        - description (string)
        - price (number)
        - stock (number)

- Retrieve all products (Admin only) (GET)
	- http://localhost:4000/products/all
    - request body: none

- Retrieve active products (GET)
    - http://localhost:4000/products/
    - request body: none

- Retrieve single product products (GET)
    - http://localhost:4000/products/64dcdfd2b8671e1685125c44
    - request body: none

- Update Product information (Admin only) (PUT)
    - http://localhost:4000/products/64dcdfd2b8671e1685125c44
    - request body: 
        - brand: (string)
        - model: (string)
        - size: (string)
        - description: (string)
        - price: (number)
        - stock: (number)

- Archive/Activate Product (Admin only) (PUT)
   - http://localhost:4000/products/64dcdfd2b8671e1685125c44/status
   - request body:
        - isActive: (boolean)
