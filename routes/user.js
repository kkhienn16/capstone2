// Route
// Dependencies and modules
const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

//Routing Component
const router = express.Router();


// Register user
router.post("/register",(req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// User Authentication
router.post("/login",userController.loginUser);

//Route to create order
router.post("/checkout",verify ,userController.order);


//Retrieve user details
router.get("/details",verify, userController.getUserDetails);

//Update Admin route
router.put('/updateAdmin', verify, verifyAdmin, userController.updateUserAsAdmin);

// Retrieve authenticated users orders
router.get('/orders', verify, userController.getUserOrders);

// Retrieve all orders (admin only)
router.get('/allOrders', verify, verifyAdmin, userController.getAllOrders);

// Add to cart (Authenticated User)
router.post('/cart/:productId',verify, userController.addToCart);

// update cart item quantity (authenticated User)
router.put('/cart/update/:productId', verify, userController.updateCartItemQuantity);

// Remove items from cart (authenticated User)
router.delete('/cart/delete/:productId', verify, userController.deleteItem);

// calculating the sum of all items in cart
router.get('/cart',verify , userController.total)

// POST route for resetting the password
router.post('/reset-password', verify, userController.resetPassword);

// Update user profile route
router.put('/profile', verify, userController.updateProfile);


// Export Route System
module.exports = router;