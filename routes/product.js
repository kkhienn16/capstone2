// Dependencies and Modules
const express = require("express");
const productController = require("../controllers/product");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

 //Routing Component
const router = express.Router();

// Create a product POST
router.post("/",verify,verifyAdmin,productController.addProduct);

// route for retrieving active products
router.get("/",productController.getAllActiveProducts);

// route for retrieving all products
router.get("/all",verify,verifyAdmin,productController.getAllProducts);

// retrieve single product
router.get("/:productId",productController.getProduct);

// updating product information
router.put("/:productId",verify, verifyAdmin,productController.updateProduct);

// archive or activate product
router.put("/:productId/status",verify, verifyAdmin,productController.productAvailabilty);

// Route to search for courses by price range
router.post('/search-by-price', productController.filterProductByPrice);

// Route for searching products by name
router.post('/search', productController.searchProductsByName);




// Export Route System
module.exports = router;