// Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Allows the backend application to be available to our frontend application
const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")


// Environment Setup
const port = 4000;

// Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// Database Connection
	// Connect to our MongoDB Database
	mongoose.connect("mongodb+srv://admin2:IeM8DXZqPFEhtr8I@batch-297.h7rcrug.mongodb.net/E-commerce?retryWrites=true&w=majority",
		{
			useNewUrlParser:true,
			useUnifiedTopology:true
		}
	)

// prompt

	let db = mongoose.connection;
	db.on('error', console.error.bind(console,'Connection error'));
	db.once('open', () => console.log('Connected to MongoDB Atlas.'));
	
	//[Backend Routes]
	//http://localhost:4000/users
	app.use("/users",userRoutes);
	app.use("/products", productRoutes);


//Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, ()=>{
		console.log(`API is now online on port ${process.env.PORT || port}`)
	})
}


module.exports = {app,mongoose};
